

# Setup

```bash
TYPE=magento-2
git init -qqq
git remote add origin https://github.com/markoshust/docker-magento.git
git fetch origin -qqq
git checkout origin/master -- compose/$TYPE
mv compose/$TYPE/* ./
mv compose/$TYPE/.vscode ./
rm -rf compose .git
git init


docker-compose up
bin/setup localhost
# if you need sample data
bin/magento sampledata:deploy
bin/magento setup:upgrade
```

# Plugin Creation

1. Bootstrap a module with a composer.json according the guidelines to ./plugins
```json
{
    "name": "magento/module-sample-shipping-provider",
    "description": "Demonstrates creation of in-store pickup shipping provider",
    "require": {
        "php": "~7.2",
        "magento/module-config": "*",
        "magento/module-store": "*",
        "magento/module-shipping": "*",
        "magento/module-quote": "*",
        "magento/framework": "*"
    },
    "type": "magento2-module",
    "version": "0.0.1",
    "license": [
        "OSL-3.0",
        "AFL-3.0"
    ],
    "autoload": {
        "files": [ "registration.php" ],
        "psr-4": {
            "Magento\\SampleShippingProvider\\": ""
        }
    }
}
```
2. Add the plugin to src/composer.json
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://repo.magento.com/"
        },
        {
            "type": "path",
            "url": "/plugins/<path to plugin>"
        }
    ]
}
```
3. Run `bin/composer require <plugin name> @dev`